# focus-spa

Para este desafio utilice digital ocean como proveedor.

Para la infraestructura, utilizo terraform. para el terraform.state utilizo un s3 bucket en el mismo proveedor para poder administrar la infraestructura de donde sea.

No es necesario definir como variable de entorno la kubeconfig ya que no es estatica, si el servicio se da de baja y se vuelve a crear, con doctl obtengo la kubeconfig, el nombre del cluster siempre es el mismo y esta almacenado en una variable.

Para desplegarlo a mi cluster utilizo helm, el proyecto compartido en test-focus.

Puede acceder al servicio
http://104.248.111.174/