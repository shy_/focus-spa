variable "CLUSTER_NAME" {}

resource "digitalocean_kubernetes_cluster" "focus-spa" {
  name    = "${var.CLUSTER_NAME}"
  region  = "nyc1"
  version = "1.24.4-do.0"

  node_pool {
    name       = "focus-spa-nodes"
    size       = "s-1vcpu-2gb"
    node_count = 1
  }
}